package com.devcamp.javabasic.s50;

public class Task5050 {
    public static void autoBoxing(){
        byte bte = 11;
        short sh = 22;
        int it = 33;
        long lng = 44;
        float fat = 55.0f;
        double dbl = 66.0d;
        char ch = 'h';
        boolean bool = false;
        
        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;

        System.out.println("Primitive Data Types sang Wrapper:");
        System.out.println("Byte object: " + byteobj);
        System.out.println("Short object: " + shortobj);
        System.out.println("Integer object: " + intobj);
        System.out.println("Long object: " + longobj);
        System.out.println("Float object: " + floatobj);
        System.out.println("Double object: " + doubleobj);
        System.out.println("Character object: " + charobj);
        System.out.println("Boolean object: " + boolobj);
    }

    public static void unBoxing(){
        byte bte = 1;
        short sh = 2;
        int it = 3;
        long lng = 4;
        float fat = 5.0f;
        double dbl = 6.0d;
        char ch = 'H';
        boolean bool = true;
        
        Byte byteobj = bte;
        Short shortobj = sh;
        Integer intobj = it;
        Long longobj = lng;
        Float floatobj = fat;
        Double doubleobj = dbl;
        Character charobj = ch;
        Boolean boolobj = bool;
        /***
         * Unboxing: Converting object to primitives
         */
        byte bytevalue = byteobj;
        short shortvalue = shortobj;
        int intvalue = intobj;
        long longvalue = longobj;
        float floatvalue = floatobj;
        double doublevalue = doubleobj;
        char charvalue = charobj;
        boolean boolvalue = boolobj;
        System.out.println("Wrapper sang PrimitiveData Types: ");
        System.out.println("byte value: " + bytevalue);
        System.out.println("short value: " + shortvalue);
        System.out.println("int value: " + intvalue);
        System.out.println("long value: " + longvalue);
        System.out.println("float value: " + floatvalue);
        System.out.println("double value: " + doublevalue);
        System.out.println("char value: " + charvalue);
        System.out.println("boolean value: " + boolvalue);
    }
    
    public static void main(String[] asgr){
        Task5050.autoBoxing();
        Task5050.unBoxing();
    }
}
